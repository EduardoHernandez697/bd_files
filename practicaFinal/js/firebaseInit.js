var config = {
  apiKey: "AIzaSyDFXqWQ9IRi1F5nsSjYKT6Q_3jZuFoHVIA",
  authDomain: "saes-dbd.firebaseapp.com",
  databaseURL: "https://saes-dbd.firebaseio.com",
  projectId: "saes-dbd",
  storageBucket: "saes-dbd.appspot.com",
  messagingSenderId: "604753401891"
};

firebase.initializeApp(config);

// Initialize Cloud Firestore through Firebase
var db = firebase.firestore();

// Disable deprecated features
db.settings({
    timestampsInSnapshots: true
});
