
$("#edit-personal").hide();
$("#save-info").hide();
$("#info-alumnos").hide();


/*
  Variables a usar
*/
let boletas = [];
var boleta;

function editPersonalInfo() {
    $("#edit-personal").show();
    $("#save-info").show();
    $("#edit-info").hide();
}


var mat = [];
var materias = [];
var html_;
function extrae_materias(){
  db.collection("Escuela /jBPCFdwLmr9eKF3E1snp/Alumnos/")
  .doc(boleta).get().then(function(doc) {
      if (doc.exists) {
          //console.log("Document data:", doc.data());
          mat.push(doc.data());
      } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
      }
      // Se extraen las materias que se están cursando
      var i;
      for (i = 0; i < mat[0]["Materias cursando"].length; i++) {
          materias = materias + "<tr><th scope=\"row\">2TM3</th> <td>"+String(mat[0]["Materias cursando"][i])+"</td><td>9</td><td>-</td>  <td>-</td><td>9</td><td>9</td></tr>";
      }
      $("#MateriAss").html(materias);
  }).catch(function(error) {
      console.log("Error getting document:", error);
  });
}


var as;
function buscarBoleta(){
    boleta = $('#inputBoleta').val();
    //console.log('Res: ',boletas,'--',boleta,'--',boletas.includes(boleta));
    //console.log(boletas.includes(boleta));
    //var existe = existe_boleta()

    //console.log(existe_boleta());



    if(boleta.length < 2 ){ //Si no se ingresa bien la boleta
      $('#inputBoleta').val('');
      alert('kmra, pon tu boleta bien[' + boleta+']');

    }else{ //Si se encuentran resultados en la BD

      $('#buscar-boleta').hide();
      $('#inputBoleta').hide();
      $("#info-alumnos").show();
      //existe_boleta(boleta);





      db.collection("Escuela /jBPCFdwLmr9eKF3E1snp/Alumnos").get()
      .then(function(querySnapshot) {
          querySnapshot.forEach(function(doc) {
              // doc.data() is never undefined for query doc snapshots
              //console.log(doc.id, " => ", doc.data());
              boletas.push(doc.id);
          });
          setTimeout(function(){
            as = boletas.slice();    //voila!
            console.log('Resas: ',as,'--',boleta,'--',as.includes(boleta));

            if(as.includes(boleta)){
              extrae_info();
              extrae_materias();
            }else{
              location.reload();
              alert('Tu boleta no existe     [' + boleta+']');
            }

          }, 100);
      })
      .catch(function(error) {
          console.log("Error getting documents: ", error);
      });






      // Consultando si existe la boleta en el SAES
      //setTimeout(existe_boleta(), 1000);
      /*if(boletas.includes(boleta)){
        console.log('Vientos perro');
      }else{
        console.log('Chale');
      }*/



    }

}


function existe_boleta(){
  // Extrayendo las boletas del SAES
var res;
  db.collection("Escuela /jBPCFdwLmr9eKF3E1snp/Alumnos").get()
  .then(function(querySnapshot) {
      querySnapshot.forEach(function(doc) {
          // doc.data() is never undefined for query doc snapshots
          //console.log(doc.id, " => ", doc.data());
          boletas.push(doc.id);
      });



      setTimeout(function(){
        as = boletas.slice();    //voila!
        //console.log('Resas: ',as,'--',boleta,'--',as.includes(boleta));
        res = as.includes(boleta);
        /*if(as.includes(boleta)){
          //console.log('Verdadero');
          return 1;
        }else{
          console.log('MALO');
          return 0;
        }*/
      }, 100);





  })
  .catch(function(error) {
      console.log("Error getting documents: ", error);
  });

return res;

}





function extrae_info(){
  db.collection("Escuela /jBPCFdwLmr9eKF3E1snp/Alumnos/")
  .doc(boleta).get().then(function(doc) {
      if (doc.exists) {
          //console.log("Document data:", doc.data());
          mat.push(doc.data());

          $("#iBoleta").html(mat[0]["Boleta"]),
          $("#iNombre").html(mat[0]["Nombre "]),
          $("#iApellidos").html(mat[0]["Apellidos"]),
          $("#iCarrera").html(mat[0]["Carrera"]),
          $("#iEstado").html(mat[0]["Regular"]);
          $("#iCorreo").html(mat[0]["Contacto"]["Correo"]);
          $("#iCURP").html(mat[0]["CURP"]);
          $("#iTelefono").html(mat[0]["Contacto"]["Telefono"]);
          $("#iCelular").html(mat[0]["Contacto"]["Celular"]);

      } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
      }
});

}


function savePersonalInfo(){
    console.log('Res: ',boletas,'--',boleta,'--',boletas.includes(boleta));

    $("#edit-personal").hide();
    $("#save-info").hide();
    $("#edit-info").show();



    $("#iCorreo").html($('#inputEmail4').val());
    $("#iCURP").html($('#inputCURP').val());
    $("#iTelefono").html($('#inputTel').val());
    $("#iCelular").html($('#inputCell').val());

    // Actualizando datos
    db.collection('Escuela /jBPCFdwLmr9eKF3E1snp/Alumnos/')
    .doc(boleta).update({
      Telefono: $('#inputTel').val(),
      CURP: $('#inputCURP').val(),
      Contacto: {
        Celular: $('#inputCell').val(),
        Correo: $('#inputEmail4').val(),
        Telefono: $('#inputTel').val()
      }
    });
    //location.reload();

    //$("#info-alumnos").load();
    $("#info-alumnos").hide();
    $("#info-alumnos").show();

}
