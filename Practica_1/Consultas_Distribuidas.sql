----- CONSULTAS FRAGMENTADAS
/*
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
-- PARA LLEVAR A CABO VARIAS CONSULTAS DISTRIIBUIDAS
create procedure sp_consulta1
-- @srvl		servidor vinculado
 @srvl	varchar (15)
 as 
begin 
---- 4.1 para REGION 1
 declare @consulta1 varchar (1000)
 set @consulta1='
	select ProductID,count(ProductID) as producto_region from(
select distinct  SD.ProductID,ST.CountryRegionCode
	from ' + @srvl + '.Servidor.dbo.SOD AS SD
	join ' + @srvl + '.Servidor.dbo.soh_reg as SO
	on SO.SalesOrderID = SD.SAlesOrderID
	join Servidor_Ctrl.AdventureWorks2014.production.Product as P
	ON SD.ProductID= P.ProductID
	--c_NF no cambia
	join Servidor_Ctrl.AdventureWorks2014.Sales.SalesTerritory as ST 
	ON SO.TerritoryID=ST.TerritoryID 
	) as ALGO
	group by ProductID
	having count(ProductID)=1'

exec (@consulta1)

end


/*
	SP consulta1
*/
go
create procedure consulta1
as
begin
	create table #tmp1(
		ProductID int,
		producto_region int
	)	
	declare @name_maquina nvarchar(20)
	declare @index int
	set @index = 1
	declare query1 cursor for select 
	name from 
		sys.servers
	as nom_maq
	open query1
	fetch next from query1 into @name_maquina 
	while @@FETCH_STATUS = 0
	begin
		if(@index > 1 and @index <= 6)
		begin
			insert into #tmp1
			exec sp_consulta1 @name_maquina
			print CAST(@name_maquina AS NTEXT)
		end		
		set @index = @index + 1;
		fetch next from query1 into @name_maquina
	end
	close query1
	deallocate query1

	select q1.* from (

	select ProductID,count(ProductID) as producto_region from(
select distinct  SD.ProductID,ST.CountryRegionCode
	from Servidor.dbo.SOD AS SD
	join Servidor.dbo.soh_reg as SO
	on SO.SalesOrderID = SD.SAlesOrderID
	join Servidor_Ctrl.AdventureWorks2014.production.Product as P
	ON SD.ProductID= P.ProductID
	--c_NF no cambia
	join Servidor_Ctrl.AdventureWorks2014.Sales.SalesTerritory as ST 
	ON SO.TerritoryID=ST.TerritoryID 
	) as ALGO
	group by ProductID
	having count(ProductID)=1

	) as q1
	left join #tmp1 on (q1.ProductID = #tmp1.ProductID)
	where #tmp1.ProductID is null

end

--drop procedure consulta1

--exec consulta1

/*
	////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

-- Consulta 2
go
create procedure sp_consulta2 
	@srv2 varchar(15)

	as
	begin
	declare @consulta2 varchar(1000)
	set @consulta2='
	select CountryRegionCode, sum(TotalDue) as Monto_total from(
	select SOH.SalesOrderID, SOH.TerritoryID, ST.Name, ST.CountryRegionCode, SOH.TotalDue from (
		' + @srv2+'.Servidor.dbo.soh_reg as SOH
		join Servidor_Ctrl.AdventureWorks2014.Sales.SalesTerritory as ST on 
			SOH.TerritoryID = ST.TerritoryID)) as V1
			group by CountryRegionCode'
exec (@consulta2)
end
-- drop procedure sp_consulta2
--exec sp_consulta2 'Maquina_1'

/*
-- Tabla temporal de la sunsulta 2
drop table #tmp2
create table #tmp2
(
	CountryRegionCode nvarchar(4),
	Monto_total float
)
-- Concatenando cada secci�n
go
declare @name_maquina2 nvarchar(20)
declare @index2 int
set @index2 = 1
declare query2 cursor for select 
name from 
	sys.servers
as nom_maq
open query2
fetch next from query2 into @name_maquina2 
while @@FETCH_STATUS = 0
begin
	if(@index2 > 1 and @index2 <= 6)
	begin
		insert into #tmp2
		exec sp_consulta2 @name_maquina2
		--print CAST(@name_maquina AS NTEXT)
	end		
	set @index2 = @index2 + 1;
	fetch next from query2 into @name_maquina2
end
close query2
deallocate query2

select * from #tmp2
*/



/*
	SP para la consulta 2
*/
go
create procedure consulta2
as
begin 
	create table #tmp2(
		CountryRegionCode nvarchar(4),
		Monto_total float
	)
	declare @name_maquina2 nvarchar(20)
	declare @index2 int
	set @index2 = 1
	declare query2 cursor for select 
	name from 
		sys.servers
	as nom_maq
	open query2
	fetch next from query2 into @name_maquina2 
	while @@FETCH_STATUS = 0
	begin
		if(@index2 > 1 and @index2 <= 6)
		begin
			insert into #tmp2
			exec sp_consulta2 @name_maquina2
			--print CAST(@name_maquina AS NTEXT)
		end		
		set @index2 = @index2 + 1;
		fetch next from query2 into @name_maquina2
	end
	close query2
	deallocate query2
	select * from #tmp2
end

-- exec consulta2


/*
	/////////////////////////////////////////////////////////////////////////////////////////////
*/
--drop procedure sp_consulta3 
create procedure sp_consulta3 
	@srv3 varchar(15)

	as
	begin
	declare @consulta3 varchar(1000)
	set @consulta3='
	select distinct p.FirstName, p.MiddleName, p.LastName 
		from Servidor_ctrl.AdventureWorks2014.dbo.PPView as p
		join Servidor_ctrl.AdventureWorks2014.Sales.Customer as sc
		on p.BusinessEntityID = sc.PersonID
		join '+@srv3+'.Servidor.dbo.soh_reg as soh
		on sc.CustomerID = soh.CustomerID and sc.TerritoryID != soh.TerritoryID
		order by p.FirstName, p.MiddleName, p.LastName;
	'
exec (@consulta3)
end

--exec sp_consulta3 'Maquina_5'

/*
	SP para consulta 3
*/
-- drop procedure consulta4
go
create procedure consulta3
as
begin
	create table #tmp3(
		FirstName nvarchar(50), 
		MiddleName nvarchar(50),
		LastName nvarchar(50)
	)
	declare @name_maquina as nvarchar(20)
	declare @index as int
	set @index = 1
	declare query3 cursor for select 
	name from 
		sys.servers
	as nom_maq
	open query3
	fetch next from query3 into @name_maquina 
	while @@FETCH_STATUS = 0
	begin
		if(@index > 1 and @index <= 6)
		begin
			insert into #tmp3
			exec sp_consulta3 @name_maquina
			print CAST(@name_maquina AS NTEXT)
		end		
		set @index = @index + 1;
		fetch next from query3 into @name_maquina
	end
	close query3
	deallocate query3

	select * from #tmp3
end

-- exec consulta3

/*
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////7
*/

--drop procedure sp_consulta4 
go
create procedure sp_consulta4 
	@srv4 varchar(15)

	as
	begin
	declare @consulta4 varchar(1000)
	set @consulta4='
	select V2.TerritoryID, V2.Repeticiones from(
select V1.TerritoryID, count(distinct V1.ProductID) as Repeticiones from (
	select SOD.ProductID, SOD.SpecialOfferID, SOH.TerritoryID
	from ' +@srv4+'.Servidor.dbo.soh_reg as SOH
		join ' +@srv4+'.Servidor.dbo.SOD as SOD
		on SOH.SalesOrderID = SOD.SalesOrderID and SOD.SpecialOfferID != 1) as V1
		group by V1.TerritoryID ) as V2
		inner join (select Repeticiones, count(*) alias_subq
					from ( select V1.TerritoryID, count(distinct V1.ProductID) as Repeticiones from (
						   select SOD.ProductID, SOD.SpecialOfferID, SOH.TerritoryID
							from '+ @srv4+'.Servidor.dbo.soh_reg as SOH
							join '+ @srv4+'.Servidor.dbo.SOD as SOD
							on SOH.SalesOrderID = SOD.SalesOrderID and SOD.SpecialOfferID != 1) as V1
							group by V1.TerritoryID ) as menos
							group by Repeticiones ) alias2 on V2.Repeticiones = alias2.Repeticiones 
															and alias2.alias_subq > 1
															order by V2.Repeticiones
	'
exec (@consulta4)
end
--exec sp_consulta4 'Maquina_5'

/*
	SP para consulta 4
*/
-- drop procedure consulta4
go
create procedure consulta4
as
begin
	create table #tmp4(
		TerritoryID int, 
		Repeticiones int
	)
	declare @name_maquina as nvarchar(20)
	declare @index as int
	set @index = 1
	declare query4 cursor for select 
	name from 
		sys.servers
	as nom_maq
	open query4
	fetch next from query4 into @name_maquina 
	while @@FETCH_STATUS = 0
	begin
		if(@index > 1 and @index <= 6)
		begin
			insert into #tmp4
			exec sp_consulta4 @name_maquina
			print CAST(@name_maquina AS NTEXT)
		end		
		set @index = @index + 1;
		fetch next from query4 into @name_maquina
	end	 
	close query4
	deallocate query4

	select * from #tmp4
end

--exec consulta4




