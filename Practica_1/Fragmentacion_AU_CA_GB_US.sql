use AdventureWorks2014
--select TerritoryID, SalesOrderID from Sales.SalesOrderHeader
go
select CountryRegionCode, COUNT(*) as TotalVentas
from (
Select SalesOrderID, CountryRegionCode from Sales.SalesOrderHeader as soh, Sales.SalesTerritory as st
where soh.TerritoryID = st.TerritoryID) as algo group by CountryRegionCode



-- ----------------------------------Fragmento para la regi�n AU-------------------------------
--drop table soh_regAU

Select soh.* into soh_regAU
from Sales.SalesOrderHeader as soh, Sales.SalesTerritory as st
where soh.TerritoryID = st.TerritoryID AND st.CountryRegionCode = 'AU'
go
select * from soh_regAU
go

Select sod.* into SOD_1
from Sales.SalesOrderDetail as sod join soh_regAU on
sod.SalesOrderID = soh_regAU.SalesOrderID
go
select SOHSR.* into SOHSR_1
from Sales.SalesOrderHeaderSalesReason as SOHSR join soh_regAU on
SOHSR.SalesOrderID = soh_regAU.SalesOrderID


--------------------------------------------------------------------------------------------------------------------
-- ----------------------------------Fragmento para la regi�n CA-------------------------------
--drop table soh_regCA
--create database Servidor_CA
-use Servidor_CA
Select soh.* into soh_regCA
from Sales.SalesOrderHeader as soh, Sales.SalesTerritory as st
where soh.TerritoryID = st.TerritoryID AND st.CountryRegionCode = 'CA'
go
select * from soh_regCA
go

Select sod.* into SOD_CA
from Sales.SalesOrderDetail as sod join soh_regCA on
sod.SalesOrderID = soh_regCA.SalesOrderID
go
select SOHSR.* into SOHSR_CA
from Sales.SalesOrderHeaderSalesReason as SOHSR join soh_regCA on
SOHSR.SalesOrderID = soh_regCA.SalesOrderID


--------------------------------------------------------------------------------------------------------------------

-- ----------------------------------Fragmento para la regi�n GB-------------------------------


--create database Servidor_GB
use Servidor_GB
Select soh.* into soh_regGB
from AdventureWorks2014.Sales.SalesOrderHeader as soh, AdventureWorks2014.Sales.SalesTerritory as st
where soh.TerritoryID = st.TerritoryID AND st.CountryRegionCode = 'GB'

Select sod.* into SOD_GB
from AdventureWorks2014.Sales.SalesOrderDetail as sod join soh_regGB on
sod.SalesOrderID = soh_regGB.SalesOrderID


select SOHSR.* into SOHSR_GB
from AdventureWorks2014.Sales.SalesOrderHeaderSalesReason as SOHSR join soh_regGB on
SOHSR.SalesOrderID = soh_regGB.SalesOrderID


--------------------------------------------------------------------------------------------------------------------

-- ----------------------------------Fragmento para la regi�n US-------------------------------


--create database Servidor_US
use Servidor_US
Select soh.* into soh_regUS
from AdventureWorks2014.Sales.SalesOrderHeader as soh, AdventureWorks2014.Sales.SalesTerritory as st
where soh.TerritoryID = st.TerritoryID AND st.CountryRegionCode = 'US'

Select sod.* into SOD_US
from AdventureWorks2014.Sales.SalesOrderDetail as sod join soh_regUS on
sod.SalesOrderID = soh_regUS.SalesOrderID


select SOHSR.* into SOHSR_US
from AdventureWorks2014.Sales.SalesOrderHeaderSalesReason as SOHSR join soh_regUS on
SOHSR.SalesOrderID = soh_regUS.SalesOrderID


--------------------------------------------------------------------------------------------------------------------