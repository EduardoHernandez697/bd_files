
/*
	Instancia SQLPAEZ - Maquina Central
	Conexiones de BD Central 
	-> Servidor DE
	-> Servidor FR
*/

-- Creando instancia a Maquina_0
go
sp_addlinkedserver @server = N'Maquina_0', @srvproduct = N'SQLServer', @provider = N'SQLOLEDB', @datasrc = N'192.168.43.37\SQLEXPRESS'
go
sp_addlinkedsrvlogin
@rmtsrvname = N'Maquina_0',-- Nombre del servidor vinculdo
@useself =  'FALSE',-- Var. booleana. TRUE si es localhost
@locallogin = N'pooe97__', -- Nombre del usuario dentro del servidor origen
@rmtuser = N'sa1',-- Nombre del usuario destino
@rmtpassword  = N'SqlServer2017'-- Pwd del usuario remoto

select * from Maquina_0.AdventureWorks2014.Sales.SalesOrderHeader

-- Creando instancia a Maquina_1
go
sp_addlinkedserver @server = N'Maquina_1', @srvproduct = N'SQLServer', @provider = N'SQLOLEDB', @datasrc = N'192.168.43.37\SQLSERVER_2'
go
sp_addlinkedsrvlogin
@rmtsrvname = N'Maquina_1',-- Nombre del servidor vinculdo
@useself =  'FALSE',-- Var. booleana. TRUE si es localhost
@locallogin = N'pooe97__', -- Nombre del usuario dentro del servidor origen
@rmtuser = N'sa',-- Nombre del usuario destino
@rmtpassword  = N'SqlServer2017'-- Pwd del usuario remoto

select * from Maquina_1.Servidor_CA.dbo.SOD_CA


-- Creando instancia local a Maquina_2
go
sp_addlinkedserver @server = N'Maquina_2', @srvproduct = N'SQLServer', @provider = N'SQLOLEDB', @datasrc = N'192.168.0.7\SQLPRODUCTION'
go
sp_addlinkedsrvlogin
@rmtsrvname = N'Maquina_2',-- Nombre del servidor vinculdo
@useself =  'FALSE',-- Var. booleana. TRUE si es localhost
@locallogin = N'pooe97__', -- Nombre del usuario dentro del servidor origen
@rmtuser = N'pooe97',-- Nombre del usuario destino
@rmtpassword  = N'Upiita2018'-- Pwd del usuario remoto
go
--select * from Servidor_FR.production.Production.Culture
select * from Maquina_2.Servidor_FR.dbo.SOD_FR


-- Creando instancia local a Maquina_3
go
sp_addlinkedserver @server = N'Maquina_3', @srvproduct = N'SQLServer', @provider = N'SQLOLEDB', @datasrc = N'192.168.0.7\SQLEXPRESS'
go
sp_addlinkedsrvlogin
@rmtsrvname = N'Servidor_DE',-- Nombre del servidor vinculdo
@useself =  'FALSE',-- Var. booleana. TRUE si es localhost
@locallogin = N'pooe97__', -- Nombre del usuario dentro del servidor origen
@rmtuser = N'pooe97_',-- Nombre del usuario destino
@rmtpassword  = N'Upiita2018'-- Pwd del usuario remoto
go
--select * from Servidor_FR.production.Production.Culture
select * from Servidor_DE.Servidor_DE.dbo.SOD_DE


-- Creando instancia a Maquina_4
go
sp_addlinkedserver @server = N'Maquina_4', @srvproduct = N'SQLServer', @provider = N'SQLOLEDB', @datasrc = N'192.168.43.37\SQLEXPRESS3'
go
sp_addlinkedsrvlogin
@rmtsrvname = N'Maquina_4',-- Nombre del servidor vinculdo
@useself =  'FALSE',-- Var. booleana. TRUE si es localhost
@locallogin = N'pooe97__', -- Nombre del usuario dentro del servidor origen
@rmtuser = N'sa',-- Nombre del usuario destino
@rmtpassword  = N'SqlServer2017'-- Pwd del usuario remoto

select * from Maquina_4.TEST3.dbo.Hola

-- Creando instancia a Maquina_5
go
sp_addlinkedserver @server = N'Maquina_5', @srvproduct = N'SQLServer', @provider = N'SQLOLEDB', @datasrc = N'192.168.43.37\SQLEXPRESS4'
go
sp_addlinkedsrvlogin
@rmtsrvname = N'Maquina_5',-- Nombre del servidor vinculdo
@useself =  'FALSE',-- Var. booleana. TRUE si es localhost
@locallogin = N'pooe97__', -- Nombre del usuario dentro del servidor origen
@rmtuser = N'sa',-- Nombre del usuario destino
@rmtpassword  = N'SqlServer2017'-- Pwd del usuario remoto

select * from Maquina_5.TEST1.dbo.Hola2
