use AdventureWorks2017

select distinct CountryRegionCode from sales.SalesTerritory.

--sp_addlinkedserver @server,@serverproduct,@provider,@datasrc
go

sp_addlinkedserver @server=N'ELPAEZ',
@srvproduct=N'SQLServer',
@provider=N'SQLOLEDB',
@datasrc=N'192.168.1.67\INSTANCIA'

--Asociar cuenta usuario al equipo remoto

--sp_addlinkedsrvlogin @rmtsrvname,@useself,@locllogin,@rmtuser,@rmtpassword
go
sp_addlinkedsrvlogin 
@rmtsrvname = N'ELPAEZ', --Nombre Servidor Vinculado
@useself = true, -- Variable tipo bool ; true (Usuario local, es mismo usuario remoto, mismas credenciales), false
@locallogin = N'' , --Nombre del usuario del servidor origen
@rmtuser = N'sa', --Nombre del usuario en el servidor destino
@rmtpassword = N''--Password del usuario remoto


use