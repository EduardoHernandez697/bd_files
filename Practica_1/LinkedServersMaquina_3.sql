/*
	Instancia SQLEXPRESS - Maquina 3
	Conexiones de Regi�n DE 
	-> Servidor FR
	-> Servidor Central
*/

-- Creando instancia local a Maquina_0
go
sp_addlinkedserver @server = N'Maquina_0', @srvproduct = N'SQLServer', @provider = N'SQLOLEDB', @datasrc = N'192.168.43.37\SQLEXPRESS'
go
sp_addlinkedsrvlogin
@rmtsrvname = N'Maquina_0',-- Nombre del servidor vinculdo
@useself =  'FALSE',-- Var. booleana. TRUE si es localhost
@locallogin = N'pooe97_', -- Nombre del usuario dentro del servidor origen
@rmtuser = N'sa1',-- Nombre del usuario destino
@rmtpassword  = N'SqlServer2017'-- Pwd del usuario remoto


-- Creando instancia local a Maquina_1
go
sp_addlinkedserver @server = N'Maquina_1', @srvproduct = N'SQLServer', @provider = N'SQLOLEDB', @datasrc = N'192.168.43.37\SQLSERVER_2'
go
sp_addlinkedsrvlogin
@rmtsrvname = N'Maquina_1',-- Nombre del servidor vinculdo
@useself =  'FALSE',-- Var. booleana. TRUE si es localhost
@locallogin = N'pooe97_', -- Nombre del usuario dentro del servidor origen
@rmtuser = N'sa',-- Nombre del usuario destino
@rmtpassword  = N'SqlServer2017'-- Pwd del usuario remoto

select * from Maquina_1.Servidor_CA.dbo.SOD_CA

-- Creando instancia local a Maquina_2
go
sp_addlinkedserver @server = N'Maquina_2', @srvproduct = N'SQLServer', @provider = N'SQLOLEDB', @datasrc = N'192.168.0.7\SQLPRODUCTION'
go
sp_addlinkedsrvlogin
@rmtsrvname = N'Maquina_2',-- Nombre del servidor vinculdo
@useself =  'FALSE',-- Var. booleana. TRUE si es localhost
@locallogin = N'pooe97_', -- Nombre del usuario dentro del servidor origen
@rmtuser = N'pooe97',-- Nombre del usuario destino
@rmtpassword  = N'Upiita2018'-- Pwd del usuario remoto
go

select * from Maquina_2.Servidor_FR.dbo.SOD_FR


-- Creando instancia local a Maquina_4
go
sp_addlinkedserver @server = N'Maquina_4', @srvproduct = N'SQLServer', @provider = N'SQLOLEDB', @datasrc = N'192.168.43.37\SQLEXPRESS3'
go
sp_addlinkedsrvlogin
@rmtsrvname = N'Maquina_4',-- Nombre del servidor vinculdo
@useself =  'FALSE',-- Var. booleana. TRUE si es localhost
@locallogin = N'pooe97_', -- Nombre del usuario dentro del servidor origen
@rmtuser = N'sa',-- Nombre del usuario destino
@rmtpassword  = N'SqlServer2017'-- Pwd del usuario remoto

select * from Maquina_4.TEST3.dbo.Hola

-- Creando instancia a Maquina_5
go
sp_addlinkedserver @server = N'Maquina_5', @srvproduct = N'SQLServer', @provider = N'SQLOLEDB', @datasrc = N'192.168.43.37\SQLEXPRESS4'
go
sp_addlinkedsrvlogin
@rmtsrvname = N'Maquina_5',-- Nombre del servidor vinculdo
@useself =  'FALSE',-- Var. booleana. TRUE si es localhost
@locallogin = N'pooe97_', -- Nombre del usuario dentro del servidor origen
@rmtuser = N'sa',-- Nombre del usuario destino
@rmtpassword  = N'SqlServer2017'-- Pwd del usuario remoto

select * from Maquina_5.TEST1.dbo.Hola2



-- Creando instancia local a Servidor central
go
sp_addlinkedserver @server = N'Servidor_Ctrl', @srvproduct = N'SQLServer', @provider = N'SQLOLEDB', @datasrc = N'192.168.0.7\SQLPAEZ'
go
sp_addlinkedsrvlogin
@rmtsrvname = N'Servidor_Ctrl',-- Nombre del servidor vinculdo
@useself =  'FALSE',-- Var. booleana. TRUE si es localhost
@locallogin = N'pooe97_', -- Nombre del usuario dentro del servidor origen
@rmtuser = N'pooe97__',-- Nombre del usuario destino
@rmtpassword  = N'Upiita2018'-- Pwd del usuario remoto
go
--select * from Servidor_FR.production.Production.Culture
select * from Servidor_Ctrl.AdventureWorks2014.Production.Culture