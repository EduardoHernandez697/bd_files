var config = {
    apiKey: "AIzaSyAJkutFot19-fispve8vxe3GMvIZufu5rM",
    authDomain: "bddistribuidas-ab840.firebaseapp.com",
    databaseURL: "https://bddistribuidas-ab840.firebaseio.com",
    projectId: "bddistribuidas-ab840",
    storageBucket: "bddistribuidas-ab840.appspot.com",
    messagingSenderId: "543035590583"
};

firebase.initializeApp(config);

// Initialize Cloud Firestore through Firebase
var db = firebase.firestore();

// Disable deprecated features
db.settings({
    timestampsInSnapshots: true
});
